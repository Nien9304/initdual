#include "struct.h"

void InitDual::analyzeCell(){
	//flip cell has a '_' before cell name
	//possible cell name L_R _L_R L__R _L__R
	m_left_cell_name = m_cell_name;
	if(m_left_cell_name.find_first_of('_') == 0){
		m_left_cell_name = m_cell_name.substr(1);
	}
	m_left_cell_name = m_left_cell_name.substr(0, m_left_cell_name.find_first_of('_'));
	m_right_cell_name = m_cell_name.substr(m_cell_name.find_last_of('_')+1);
}
string getLeftPinName(const string &name){
	return "L_" + name;
}
string getRightPinName(const string &name){
	return "R_" + name;
}
void InitDual::mergeCell(){
	m_cell.m_name = m_cell_name;
	m_cell.m_footprint = "dual";
	//TODO: option to switch min/max/avg
	m_cell.m_drivingStrength = (m_left_cell.m_drivingStrength + m_right_cell.m_drivingStrength) / 2;
	//m_type
	if(m_left_cell.m_type == CT_COMB && m_right_cell.m_type == CT_COMB){
		m_cell.m_type = CT_COMB;
	}
	else if(m_left_cell.m_type == CT_FF && m_right_cell.m_type == CT_FF){
		m_cell.m_type = CT_FF;
	}
	else if((m_left_cell.m_type == CT_COMB && m_right_cell.m_type == CT_FF) ||
		(m_left_cell.m_type == CT_FF && m_right_cell.m_type == CT_COMB)){
		m_cell.m_type = CT_MIXED;
	}
	else{
		m_cell.m_type = CT_INVALID;
	}
	//input pins
	for(UDT_STR_PIN_MAP::iterator pin_it = m_left_cell.m_input.begin();
	pin_it != m_left_cell.m_input.end(); ++pin_it){
		pin tmp_pin = pin_it->second;
		tmp_pin.m_name = getLeftPinName(tmp_pin.m_name);
		m_cell.m_input[tmp_pin.m_name] = tmp_pin;
	}
	for(UDT_STR_PIN_MAP::iterator pin_it = m_right_cell.m_input.begin();
	pin_it != m_right_cell.m_input.end(); ++pin_it){
		pin tmp_pin = pin_it->second;
		tmp_pin.m_name = getRightPinName(tmp_pin.m_name);
		m_cell.m_input[tmp_pin.m_name] = tmp_pin;
	}
	//output pins
	for(UDT_STR_PIN_MAP::iterator pin_it = m_left_cell.m_output.begin();
	pin_it != m_left_cell.m_output.end(); ++pin_it){
		pin tmp_pin = pin_it->second;
		tmp_pin.m_name = getLeftPinName(tmp_pin.m_name);
		m_cell.m_output[tmp_pin.m_name] = tmp_pin;
	}
	for(UDT_STR_PIN_MAP::iterator pin_it = m_right_cell.m_output.begin();
	pin_it != m_right_cell.m_output.end(); ++pin_it){
		pin tmp_pin = pin_it->second;
		tmp_pin.m_name = getRightPinName(tmp_pin.m_name);
		m_cell.m_output[tmp_pin.m_name] = tmp_pin;
	}
	//TODO: Sequential circuit need a new cell info structure
	//for mixing activate-high/low set/reset.
}
void InitDual::mergePattern(){
	unsigned pattern_idx = 0;
	//input pins
	for(vector<string>::iterator pin_it = m_left_pattern.m_inputPin.begin();
	pin_it != m_left_pattern.m_inputPin.end(); ++pin_it){
		m_pattern.m_inputPin.push_back(getLeftPinName(*pin_it));
	}
	for(vector<string>::iterator pin_it = m_right_pattern.m_inputPin.begin();
	pin_it != m_right_pattern.m_inputPin.end(); ++pin_it){
		m_pattern.m_inputPin.push_back(getRightPinName(*pin_it));
	}
	//output pins
	for(vector<string>::iterator pin_it = m_left_pattern.m_outputPin.begin();
	pin_it != m_left_pattern.m_outputPin.end(); ++pin_it){
		m_pattern.m_outputPin.push_back(getLeftPinName(*pin_it));
	}
	for(vector<string>::iterator pin_it = m_right_pattern.m_outputPin.begin();
	pin_it != m_right_pattern.m_outputPin.end(); ++pin_it){
		m_pattern.m_outputPin.push_back(getRightPinName(*pin_it));
	}
	//initialize all 1tf/2tf patterns
	m_pattern.m_truthTable.resize(
		m_left_pattern.m_truthTable.size()*m_right_pattern.m_truthTable.size());
	m_pattern.m_transitionTable.resize(
		m_left_pattern.m_truthTable.size()*m_right_pattern.m_transitionTable.size()
		+ m_left_pattern.m_transitionTable.size()*m_right_pattern.m_truthTable.size());
	//merge truth table
	pattern_idx = 0;
	for(UDT_STR_VEC::iterator left_pattern = m_left_pattern.m_truthTable.begin();
	left_pattern != m_left_pattern.m_truthTable.end(); ++left_pattern){
		for(UDT_STR_VEC::iterator right_pattern = m_right_pattern.m_truthTable.begin();
		right_pattern != m_right_pattern.m_truthTable.end(); ++right_pattern){
			//L input
			m_pattern.m_truthTable[pattern_idx] += left_pattern->substr(0, m_left_pattern.m_inputPin.size());
			//R input
			m_pattern.m_truthTable[pattern_idx] += right_pattern->substr(0, m_right_pattern.m_inputPin.size());
			//L output
			m_pattern.m_truthTable[pattern_idx] += left_pattern->substr( m_left_pattern.m_inputPin.size());
			//R output
			m_pattern.m_truthTable[pattern_idx] += right_pattern->substr( m_right_pattern.m_inputPin.size());
			++pattern_idx;
		}
	}
	//merge transition table
	pattern_idx = 0;
	for(UDT_STR_VEC::iterator left_pattern = m_left_pattern.m_truthTable.begin();
	left_pattern != m_left_pattern.m_truthTable.end(); ++left_pattern){
		string left_pattern_tr;
		left_pattern_tr.resize(left_pattern->size());
		for(size_t pos = 0; pos != left_pattern->size(); ++pos){
			switch((*left_pattern)[pos]){
			case '0':
				left_pattern_tr[pos] = 'L';
				break;
			case '1':
				left_pattern_tr[pos] = 'H';
				break;
			}
		}
		for(UDT_STR_VEC::iterator right_pattern = m_right_pattern.m_transitionTable.begin();
		right_pattern != m_right_pattern.m_transitionTable.end(); ++right_pattern){	
			//L input
			m_pattern.m_transitionTable[pattern_idx] += left_pattern_tr.substr(0, m_left_pattern.m_inputPin.size());
			//R input
			m_pattern.m_transitionTable[pattern_idx] += right_pattern->substr(0, m_right_pattern.m_inputPin.size());
			//L output
			m_pattern.m_transitionTable[pattern_idx] += left_pattern_tr.substr(m_left_pattern.m_inputPin.size());
			//R output
			m_pattern.m_transitionTable[pattern_idx] += right_pattern->substr(m_right_pattern.m_inputPin.size());
			++pattern_idx;
		}
	}
	for(UDT_STR_VEC::iterator right_pattern = m_right_pattern.m_truthTable.begin();
	right_pattern != m_right_pattern.m_truthTable.end(); ++right_pattern){
		string right_pattern_tr;
		right_pattern_tr.resize(right_pattern->size());
		for(size_t pos = 0; pos != right_pattern->size(); ++pos){
			switch((*right_pattern)[pos]){
			case '0':
				right_pattern_tr[pos] = 'L';
				break;
			case '1':
				right_pattern_tr[pos] = 'H';
				break;
			}
		}
		for(UDT_STR_VEC::iterator left_pattern = m_left_pattern.m_transitionTable.begin();
		left_pattern != m_left_pattern.m_transitionTable.end(); ++left_pattern){
			//L input
			m_pattern.m_transitionTable[pattern_idx] += left_pattern->substr(0, m_left_pattern.m_inputPin.size());
			//R input
			m_pattern.m_transitionTable[pattern_idx] += right_pattern_tr.substr(0, m_right_pattern.m_inputPin.size());
			//L output
			m_pattern.m_transitionTable[pattern_idx] += left_pattern->substr(m_left_pattern.m_inputPin.size());
			//R output
			m_pattern.m_transitionTable[pattern_idx] += right_pattern_tr.substr(m_right_pattern.m_inputPin.size());
			++pattern_idx;
		}
	}
}

void replaceName(string &name, string(*rename_fun)(const string&)){
	name = rename_fun(name);
}
void replaceNetName(string &net, 
	UDT_SET<string, caseInsensitiveHash, caseInsensitiveEqual> &pg_set,
	string (*rename_fun)(const string&)){
	if(pg_set.find(net) == pg_set.end()){
		replaceName(net, rename_fun);
	}
}
void InitDual::writeLeftSpice(ostream &os){
	string cell_name = m_left_subckt.m_description[0];
	replaceName(m_left_subckt.m_description[0], getLeftPinName);
	m_left_subckt.writeSpiceFile(os);
	m_left_subckt.m_description[0] = cell_name;
}
void InitDual::writeRightSpice(ostream &os){
	string cell_name = m_right_subckt.m_description[0];
	replaceName(m_right_subckt.m_description[0], getRightPinName);
	m_right_subckt.writeSpiceFile(os);
	m_right_subckt.m_description[0] = cell_name;
}
void InitDual::writeLvsSpice(ostream &os, const string &left_spice_filename,
	const string &right_spice_filename){
	//.include ......
	//
	//.subckt VDD GND L_ L_ R_ R_
	//Xleft VDD GND L_ L_ L_cell
	//Xright VDD GND R_ R_ R_cell
	//.ends
	os << m_lvs_include_path << endl;
	os << ".subckt";
	for(UDT_STR_VEC::iterator str_it = m_subckt.m_description.begin();
	str_it != m_subckt.m_description.end(); ++str_it){
		os << " " << *str_it;
	}
	os << endl;
	os << "Xleft";
	for(unsigned idx = 1; idx < m_left_subckt.m_description.size(); ++idx){
		if(m_pg_set.find(m_left_subckt.m_description[idx]) != m_pg_set.end()){
			os << " " << m_left_subckt.m_description[idx];
		}
		else{
			os << " " << getLeftPinName(m_left_subckt.m_description[idx]);
		}
	}
	os << " " << (m_left_subckt.m_description[0]) << endl;
	os << "Xright";
	for(unsigned idx = 1; idx < m_right_subckt.m_description.size(); ++idx){
		if(m_pg_set.find(m_right_subckt.m_description[idx]) != m_pg_set.end()){
			os << " " << m_right_subckt.m_description[idx];
		}
		else{
			os << " " << getRightPinName(m_right_subckt.m_description[idx]);
		}
	}
	os << " " << (m_right_subckt.m_description[0]) << endl;
	os << ".ends" << endl;

}
//Debug the flow without RC extraction.
void InitDual::mergeSubckt(){ 
	m_subckt.m_option = m_left_subckt.m_option;
	m_subckt.m_option.insert(m_subckt.m_option.end(), 
		m_right_subckt.m_option.begin(), m_right_subckt.m_option.end());
	//generate PG set
	for(UDT_STR_VEC::iterator str_it = m_powerList.begin();
	str_it != m_powerList.end(); ++str_it){
		m_pg_set.insert(*str_it);
	}
	for(UDT_STR_VEC::iterator str_it = m_groundList.begin();
	str_it != m_groundList.end(); ++str_it){
		m_pg_set.insert(*str_it);
	}
	//rename subckt title
	m_subckt.m_description.push_back(m_cell_name);
	for(UDT_STR_VEC::iterator str_it = m_left_subckt.m_description.begin()+1;
	str_it != m_left_subckt.m_description.end(); ++str_it){
		if(m_pg_set.find(*str_it) != m_pg_set.end()){
			m_subckt.m_description.push_back(*str_it);
		}
		else{
			m_subckt.m_description.push_back(getLeftPinName(*str_it));
		}
	}
	for(UDT_STR_VEC::iterator str_it = m_right_subckt.m_description.begin()+1;
	str_it != m_right_subckt.m_description.end(); ++str_it){
		
		if(m_pg_set.find(*str_it) != m_pg_set.end()){
			//assume pg net is the same in left and right netlist
		}
		else{
			m_subckt.m_description.push_back(getRightPinName(*str_it));
		}
	}
	//replace all instance name, all net name
	//R C = 0 1 2
	//M X = 0 1 2 3 4
	for(UDT_ELEMENT_VEC::iterator element_it = m_left_subckt.m_netlist.begin();
	element_it != m_left_subckt.m_netlist.end(); ++element_it){
		element new_element = *element_it;
		switch(element_it->m_type){
		case ET_C:
		case ET_R:
		case ET_D:
			replaceName(new_element.m_description[0], getLeftPinName);
			replaceNetName(new_element.m_description[1], m_pg_set, getLeftPinName);
			replaceNetName(new_element.m_description[2], m_pg_set, getLeftPinName);
			break;
		case ET_X:
		case ET_M:
			replaceName(new_element.m_description[0], getLeftPinName);
			replaceNetName(new_element.m_description[1], m_pg_set, getLeftPinName);
			replaceNetName(new_element.m_description[2], m_pg_set, getLeftPinName);
			replaceNetName(new_element.m_description[3], m_pg_set, getLeftPinName);
			replaceNetName(new_element.m_description[4], m_pg_set, getLeftPinName);
			break;
		default:
			cerr << "Error: Unknown element type" << endl;
			exit(-1);
		}
		m_subckt.m_netlist.push_back(new_element);
	}
	for(UDT_ELEMENT_VEC::iterator element_it = m_right_subckt.m_netlist.begin();
	element_it != m_right_subckt.m_netlist.end(); ++element_it){
		element new_element = *element_it;
		switch(element_it->m_type){
		case ET_C:
		case ET_R:
			replaceName(new_element.m_description[0], getRightPinName);
			replaceNetName(new_element.m_description[1], m_pg_set, getRightPinName);
			replaceNetName(new_element.m_description[2], m_pg_set, getRightPinName);
			break;
		case ET_X:
		case ET_M:
			replaceName(new_element.m_description[0], getRightPinName);
			replaceNetName(new_element.m_description[1], m_pg_set, getRightPinName);
			replaceNetName(new_element.m_description[2], m_pg_set, getRightPinName);
			replaceNetName(new_element.m_description[3], m_pg_set, getRightPinName);
			replaceNetName(new_element.m_description[4], m_pg_set, getRightPinName);
			break;
		default:
			cerr << "Error: Unknown element type" << endl;
			exit(-1);
		}
		m_subckt.m_netlist.push_back(new_element);
	}
}