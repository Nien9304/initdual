#include "config.h"

void config::readConfig(istream &is){
	string line, key, value;
	string::size_type pos;
	bool is_multipleLine = false;
	while(!is.eof()){
		getline(is, line);
		//multiple line
		if(is_multipleLine == true){
			if(line.find_first_of('}') != string::npos){
				is_multipleLine = false;
			}
			else{
				m_table[key] += line + '\n';
			}
			continue;
		}
		//ignore all whitespace
		line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
		//ignore empty line or comment
		if(line.empty() || !isalnum(line[0])) continue;
		pos = line.find_first_of("=");
		key = line.substr(0, pos);
		value = line.substr(pos+1);
		//transform(key.begin(), key.end(), key.begin(), ::tolower);
		if(m_table.find(key) != m_table.end()){
			cerr << "Error: in config file, redefine " << key << "=" << value << endl;
			exit(-1);
		}
		if(value.find_first_of('{') != string::npos){
			if(is_multipleLine == true){
				cerr << "Error: Unexpected {" << endl;
				exit(-1);
			}
			is_multipleLine = true;
			continue;
		}
		m_table[key] = value;
	}
	if(is_multipleLine == true){
		cerr << "Error: Unclosed }" << endl;
		exit(-1);
	}
}
void config::getCfg(const string &key, string &value){
	UDT_STR_STR_MAP::iterator it;
	it = m_table.find(key);
	if(it == m_table.end()){
		cerr << "Error: in config file, key \"" << key << "\" is not defined."  << endl;
		exit(-1);
	}
	value = it->second;
}
void config::getCfg(const string &key, bool &value){
	string _value;
	getCfg(key, _value);
	transform(_value.begin(), _value.end(), _value.begin(), ::tolower);
	if(_value == "true" || _value == "1"){
		value = true;
	}
	else if(_value == "false" || _value == "0"){
		value = false;
	}
	else{
		cerr << "Error: in config file, " << key << "=" 
			<< _value << " is not bollean value." << endl;
		exit(-1);
	}
}
void config::getCfg(const string &key, int &value){
	string _value;
	getCfg(key, _value);
	value = atoi(_value.c_str());
}
void config::getCfg(const string &key, double &value){
	string _value;
	getCfg(key, _value);
	value = atof(_value.c_str());
}
void config::getCfg(const string &key, UDT_STR_VEC &value){
	string _value;
	size_t pos, lastPos = 0;
	getCfg(key, _value);
	pos = _value.find_first_of(',', lastPos);
	while(pos != string::npos){
		value.push_back(_value.substr(lastPos, pos - lastPos));
		lastPos = pos+1;
		pos = _value.find_first_of(',', lastPos);
	}
	value.push_back(_value.substr(lastPos));
}
void config::getCfg(const string &key, UDT_SET<string> &value){
	UDT_STR_VEC _value;
	getCfg(key, _value);
	for(UDT_STR_VEC::iterator strIt = _value.begin(); strIt != _value.end(); ++strIt){
		value.insert(*strIt);
	}
}
void config::getCfg(const string &key, set<string> &value){
	UDT_STR_VEC _value;
	getCfg(key, _value);
	for(UDT_STR_VEC::iterator strIt = _value.begin(); strIt != _value.end(); ++strIt){
		value.insert(*strIt);
	}
}
