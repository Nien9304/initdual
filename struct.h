#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"
#include "config.h"
#include "struct_info.h"
#include "struct_pattern.h"
#include "struct_spice.h"

struct InitDual{
	InitDual(){};
	void setConfig(istream &is){
		m_config.readConfig(is);
		m_config.getCfg(string("setup.lvs_include_path"), m_lvs_include_path);
		m_config.getCfg(string("spice.power"), m_powerList);
		m_config.getCfg(string("spice.ground"), m_groundList);
	};
	void analyzeCell();
	//string getLeftPinName(const string &name);
	//string getRightPinName(const string &name);
	string getLeftCell(){ return m_left_cell_name; };
	string getRightCell(){ return m_right_cell_name; };
	void mergeCell();
	void mergePattern();
	void mergeSubckt();
	void writeLeftSpice(ostream &os);
	void writeRightSpice(ostream &os);
	void writeLvsSpice(ostream &os, const string &left_spice_filename,
		const string &right_spice_filename);

	UDT_SET<string, caseInsensitiveHash, caseInsensitiveEqual> m_pg_set;
	//info
	subckt m_left_subckt, m_right_subckt, m_subckt;
	cell m_left_cell, m_right_cell, m_cell;
	pattern m_left_pattern, m_right_pattern, m_pattern;

	config m_config;
	//scope
	string m_cell_name;
	//parameters;
	string m_left_cell_name, m_right_cell_name;
	string m_lvs_include_path;
	UDT_STR_VEC m_powerList, m_groundList;
};

#endif