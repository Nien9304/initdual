//
//	struct_info.h
//	struct_info.cpp
//
//	Files: .libps
//	Structure:
//	-cell
//		cell name, footprint, strength, type
//		-input pins, output pins
//			name, direction, input cap
//		-seq
//			clock, clear, preset pin name and its activation
//	
//	v1.1.2 //add write serial file function and fix pin information bug

#ifndef _STRUCT_INFO_H_
#define _STRUCT_INFO_H_
#include "common.h"

enum CELL_TYPE{CT_COMB, CT_FF, CT_LATCH, CT_MIXED, CT_INVALID};
static const char *CELL_TYPE_NAME[] = {"CT_COMB", "CT_FF", "CT_LATCH", "CT_MIXED", "CT_INVALID" };
std::istream& operator >>(std::istream &is, CELL_TYPE &type);

struct pin;
typedef UDT_MAP<string, pin> UDT_STR_PIN_MAP;

//stdcell structure
struct seq{
	seq():m_clearPresetValueP(-1),m_clearPresetValueN(-1){};
	//The following two parameters is used to determine the output value of Q Qn
	//when clear and preset are both activated.
	int m_clearPresetValueP;
	int m_clearPresetValueN;
	string m_clockPin;
	string m_clearPin;
	string m_presetPin;
	bool m_clockActivation;
	bool m_clearActivation;
	bool m_presetActivation;
};
struct pin{
	string m_name;
	//output pin, for dual cell to choose comb/ff test for this pin.
	CELL_TYPE m_type;
	//input pin, a reference to calculate cell driving strength. 
	//(Not used in current version)
	double m_inputCapacitance; 
};
struct cell{
	cell():m_drivingStrength(-1.0){};
	void readSerialInfoFile(istream &is);
	void writeSerialInfoFile(ostream &os);

	string m_name;
	string m_footprint;
	float m_drivingStrength;
	CELL_TYPE m_type;
	UDT_STR_PIN_MAP m_input;
	UDT_STR_PIN_MAP m_output;
	//seq pins
	seq m_seq;
};
#endif