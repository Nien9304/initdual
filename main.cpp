const char g_version[] = 
//
//	main.cpp
//	InitDual
//
//	Created by PCCO	@2016/04/29
"1.1.0"//Including lvs spice netlist from library
;
#include "struct.h"

int main(int argc, char **argv){
	ofstream ofs;
	ifstream ifs;
	string path;
	string filename, cell_name, left_spice_filename, right_spice_filename;
	InitDual init_dual_inst;

	if(argc != 4){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./initDual "
			 << "[working directory(./)] [cell name] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./info/" << endl;
		cerr << "../../[cell L/R]/single/info/" << endl;
		cerr << "Files read from out of the working directory:" << endl;
		cerr << "../../[cell L/R]/single/info/[cell].libps (cell info file)" << endl;
		cerr << "../../[cell L/R]/single/info/[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "../../[cell L/R]/single/info/[cell].2tfpat (2tf pattern file)" << endl;
		cerr << "../../[cell L/R]/single/info/[cell].spips (spice netlist file)" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./info/[cell].libps (cell info file)" << endl;
		cerr << "./info/[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "./info/[cell].2tfpat (2tf pattern file)" << endl;
		cerr << "./info/[cell].lvs.spi (spice file for LVS)" << endl;
		exit(-1);
	}
	path = argv[1];
	path += '/';
	init_dual_inst.m_cell_name = argv[2];
	init_dual_inst.analyzeCell();

	cout << ">> Read config." << endl;
	filename = argv[3];
	openFile(ifs, filename);
	init_dual_inst.setConfig(ifs);
	ifs.close();

	cout << ">> Read library infomation." << endl;
	cell_name = init_dual_inst.getLeftCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".libps";
	openFile(ifs, filename);
	init_dual_inst.m_left_cell.readSerialInfoFile(ifs);
	ifs.close();
	cell_name = init_dual_inst.getRightCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".libps";
	openFile(ifs, filename);
	init_dual_inst.m_right_cell.readSerialInfoFile(ifs);
	ifs.close();

	cout << ">> Read 1 time frame pattern." << endl;
	cell_name = init_dual_inst.getLeftCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".1tfpat";
	openFile(ifs, filename);
	init_dual_inst.m_left_pattern.readSerial1tfFile(ifs);
	ifs.close();
	cell_name = init_dual_inst.getRightCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".1tfpat";
	openFile(ifs, filename);
	init_dual_inst.m_right_pattern.readSerial1tfFile(ifs);
	ifs.close();

	cout << ">> Read 2 time frame pattern." << endl;
	cell_name = init_dual_inst.getLeftCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".2tfpat";
	openFile(ifs, filename);
	init_dual_inst.m_left_pattern.readSerial2tfFile(ifs);
	ifs.close();
	cell_name = init_dual_inst.getRightCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".2tfpat";
	openFile(ifs, filename);
	init_dual_inst.m_right_pattern.readSerial2tfFile(ifs);
	ifs.close();

	cout << ">> Read cell spice netlist." << endl;
	cell_name = init_dual_inst.getLeftCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".spips";
	openFile(ifs, filename);
	init_dual_inst.m_left_subckt.readSerialFile(ifs);
	ifs.close();
	cell_name = init_dual_inst.getRightCell();
	filename = path + "../../" + cell_name +
		"/single/info/" + cell_name + ".spips";
	openFile(ifs, filename);
	init_dual_inst.m_right_subckt.readSerialFile(ifs);
	ifs.close();

	init_dual_inst.mergeCell();
	init_dual_inst.mergePattern();
	//generate PG set, and merged cell title
	init_dual_inst.mergeSubckt();
	//spice netlist's scale option can not merge

	cout << ">> Write library information." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".libps";
	openFile(ofs, filename);
	init_dual_inst.m_cell.writeSerialInfoFile(ofs);
	ofs.close();
	cout << ">> Write 1tf pattern." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".1tfpat";
	openFile(ofs, filename);
	init_dual_inst.m_pattern.writeSerial1tfFile(ofs);
	ofs.close();
	cout << ">> Write 2tf pattern." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".2tfpat";
	openFile(ofs, filename);
	init_dual_inst.m_pattern.writeSerial2tfFile(ofs);
	ofs.close();
	/*
	cout << ">> Write Left spice." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".left.spi";
	left_spice_filename = filename;
	openFile(ofs, filename);
	init_dual_inst.writeLeftSpice(ofs);
	ofs.close();
	cout << ">> Write Right spice." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".right.spi";
	right_spice_filename = filename;
	openFile(ofs, filename);
	init_dual_inst.writeRightSpice(ofs);
	ofs.close();
	*/
	cout << ">> Write LVS spice." << endl;
	filename = path + "info/" + init_dual_inst.m_cell_name + ".lvs.spi";
	openFile(ofs, filename);
	init_dual_inst.writeLvsSpice(ofs, left_spice_filename, right_spice_filename);
	ofs.close();
    return 0;
} 
