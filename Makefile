TARGET = ../initDual
OBJECT = init_dual.o main.o common.o config.o struct_info.o struct_pattern.o struct_spice.o
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
#makefile knows the dependency between .o .cpp
struct.h: common.h config.h struct_info.h struct_pattern.h struct_spice.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp
config.o:config.cpp config.h
	$(CC) $(CCFLAG) -c config.cpp
struct_info.o:struct_info.cpp struct_info.h
	$(CC) $(CCFLAG) -c struct_info.cpp
struct_pattern.o:struct_pattern.cpp struct_pattern.h
	$(CC) $(CCFLAG) -c struct_pattern.cpp
struct_spice.o:struct_spice.cpp struct_spice.h
	$(CC) $(CCFLAG) -c struct_spice.cpp
	
init_dual.o:init_dual.cpp struct.h
	$(CC) $(CCFLAG) -c init_dual.cpp
main.o:main.cpp struct.h
	$(CC) $(CCFLAG) -c main.cpp
clean:
	rm -f $(TARGET) $(OBJECT)